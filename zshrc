export LANG=ja_JP.UTF-8
#export LANG=C 
HISTFILE=$HOME/.zsh-history
HISTSIZE=100000
SAVEHIST=100000

fpath=(~/.zsh/ ${fpath})
autoload -U compinit
compinit

unsetopt promptcr
setopt prompt_subst
setopt nobeep
setopt long_list_jobs
setopt list_types
setopt auto_resume
setopt auto_list
setopt hist_ignore_dups
setopt autopushd
setopt pushd_ignore_dups
setopt auto_menu
setopt extended_history
setopt equals
setopt magic_equal_subst
setopt hist_verify
setopt numeric_glob_sort
setopt print_eight_bit
setopt share_history
zstyle ':completion:*:default' menu select=1
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
setopt auto_param_keys
setopt auto_param_slash

PROMPT=$(print "%B%{\e[37m%}`whoami`@%m:%(5~,%-2~/.../%2~,%~)%{\e[37m%}%# %b")

#export LSCOLORS=exfxcxdxbxegedabagacad
#export LS_COLORS='di=34:ln=35:so=32:pi=33:ex=31:bd=46;34:cd=43;34:su=41;30:sg=46;30:tw=42;30:ow=43;30'
#alias ls="ls -G"
#alias gls="gls --color"

PATH="$HOME/.bin:"
# PATH+="/usr/local/bin:/usr/local/bin/mygcc/bin:/usr/local/bin:/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/sbin:/usr/games:"
PATH+="/usr/local/java/jre1.6.0_25/bin:"
PATH+="/usr/local/bin:/usr/local/bin:/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/sbin:/usr/games:"
PATH+="/usr/local/thunderbird:"
PATH+="/opt/crosstool/gcc-3.4.5-glibc-2.3.5/sh4-unknown-linux-gnu/bin:"
PATH+="/usr/local/bin/sh-cross-hitachi/bin:"
PATH+="/usr/local/tip-gcc/bin:"
PATH+="/usr/local/go/bin:"

# bindkey -m breaks multibyte support
# bindkey -m
# bindkey 'M-n' history-search-forward
# bindkey 'M-p' history-search-backward
# bindkey 'M-h' backward-delete-char

bindkey '^h' backward-char
bindkey '^j' history-search-forward
bindkey '^k' history-search-backward
bindkey '^l' forward-char
bindkey '^p' kill-line
bindkey '^d' delete-char-or-list

alias emacs="emacs -nw"
export EDITOR=emacs

if [ `uname` != Darwin ];
then
	alias ls="ls --color"
else
	alias ls="ls -G"
fi

alias xterm="xterm -bg black -fg white"
alias w3m="w3m -cookie"
alias mlterm="mlterm -g=120x30"
alias su="export LANG=en_US; su; export LANG=ja_JP.UTF-8"

alias mcom0="LANG=C minicom usb0"
alias mcom1="LANG=C minicom usb1"
alias mcom2="LANG=C minicom usb2"
alias mcom3="LANG=C minicom usb3"

alias top="top -d 1"

clear-screen-rehash() {
        zle clear-screen
        rehash
        zle reset-prompt
}

zle -N clear-screen-rehash
bindkey '^n' clear-screen-rehash

# alias glg="git log -p | gitless"


export PYTHONPATH=/usr/lib/python2.6/site-packages

compdef -d git

# http://stackoverflow.com/questions/1128496/to-get-a-prompt-which-indicates-git-branch-in-zsh
setopt prompt_subst
autoload -Uz vcs_info
zstyle ':vcs_info:*' actionformats \
    '%F{5}(%f%s%F{5})%F{3}-%F{5}[%F{2}%b%F{3}|%F{1}%a%F{5}]%f '
zstyle ':vcs_info:*' formats       \
    '%F{5}(%f%s%F{5})%F{3}-%F{5}[%F{2}%b%F{5}]%f '
zstyle ':vcs_info:(sv[nk]|bzr):*' branchformat '%b%F{1}:%F{3}%r'

zstyle ':vcs_info:*' enable git cvs svn

# or use pre_cmd, see man zshcontrib
vcs_info_wrapper() {
  vcs_info
  if [ -n "$vcs_info_msg_0_" ]; then
    echo "%{$fg[grey]%}${vcs_info_msg_0_}%{$reset_color%}$del"
  fi
}
RPROMPT=$'$(vcs_info_wrapper)'

alias sudo='sudo env PATH=$PATH GOPATH=$GOPATH'

PATH=$PATH:/home/mitake/gocode/bin
PATH=$PATH:/home/mitake/go/bin

JAVA_HOME=/usr/lib/jvm/java-1.7.0-openjdk-amd64          

alias mplayer="mplayer -novideo"

export GOPATH=$HOME/gopath
PATH=$PATH:$GOPATH/bin

fortune

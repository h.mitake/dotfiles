#compdef sevil.rb

_values 'sevil.rb subcommands'\
	'info[general information]' \
	'vdesk[current virtual desktop number]' \
	'windows[window list]' \
	'ignores[ignore list]' \
	'keys[current key configuration]' \
	'res[graphic resolution]' \
	'orig_keys[built-time key configuration]' \
	'orig_ignores[built-time ignore configuration]' \
	'orig_windows[built-time window configuration]' \
	'argv[arguments]' \
	'edit_argv[edit arguments]' && return

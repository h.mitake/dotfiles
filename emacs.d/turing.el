;; Boomborg/turing : Turing機械のシミュレータ
;; http://nicosia.is.s.u-tokyo.ac.jp/pub/staff/hagiya/boomborg/turing/

;; 1110100101010111

;; .aa:0?0>aa;1?0>bb; ?0<aa;
;; @bb:0?1<bb;1?1=aa; ?1<bb;

;; まず、turing.elというファイルをESC-X load-fileによってロードします。

;; そして、文字カーソルを上の1110100101010111の行のどこかに持って行き、
;; C-Rを続けて入力してみて下さい。

;; 文字カーソルはTuring機械のヘッドを意味します。
;; 文字カーソルのある行がTuring機械のテープです。

;; Turing機械のテープの後ろに続く.または@で始まる各行は、
;; Turing機械の各状態に対応します。

;; 各行は次のように解釈されます。

;; 	@bb:0?1<bb;1?1>aa; ?1<bb;
;; 	^

;; @で始まるのは現在の状態を意味します。

;; 	@bb:0?1<bb;1?1>aa; ?1<bb;
;; 	 ^^

;; bbは状態の名前です。

;; 	@bb:0?1<bb;1?1=aa; ?1<bb;
;; 	    ^^^^^^

;; 0?1<bbは、ヘッドの下の文字が0ならば、
;; ヘッドの下に1を書き込み、ヘッドを左へ動かし(<)、
;; 状態bbへ遷移することを意味します。

;; 一般に、各行は、

;; 	@または.
;; 	状態の名前
;; 	:
;; 	ヘッドの下の文字
;; 	?
;; 	ヘッドの下に書き込む文字
;; 	<(右へ動かす)か=(動かさない)か>(左へ動かす)のどれか
;; 	次の状態の名前
;; 	;
;; 	ヘッドの下の文字
;; 	?
;; 	ヘッドの下に書き込む文字
;; 	<か=か>のどれか
;; 	次の状態の名前
;; 	;
;; 	...

;; という形をしています。

;; Turing機械のテープは、左右に無限個の空白文字が続いていると
;; 考えられます。従って、各状態には、

;; 	@bb:0?1<bb;1?1=aa; ?1<bb;
;; 	                  ^^^^^^

;; のように、空白に対する処理を指定しておく必要があります。

(defun one-transition ()
  (let ((p (point)) (c (following-char)))
    (cond ((= c ?\n)
           (insert " ")
           (backward-char)
           (setq c ?\ )))
    (re-search-forward "\n@")
    (delete-char -1)
    (insert ".")
    (re-search-forward (concat (char-to-string c) "\\?"))
    (let ((nc (following-char)))
      (forward-char)
      (let ((dir (following-char)))
        (forward-char)
        (let ((q (point)))
          (re-search-forward ";")
          (let ((s (buffer-substring q (- (point) 1))))
            (goto-char p)
            (re-search-forward (concat "\n\\." s ":"))
            (re-search-backward "\\.")
            (delete-char 1)
            (insert ?@)
            (goto-char p)
            (delete-char 1)
            (insert nc)
            (cond ((= dir ?>)
                   (if (= (following-char) ?\n)
                       (progn (insert ?\ ) (backward-char))))
                  ((= dir ?<)
                   (backward-char)
                   (if (or (bobp) (= (preceding-char) ?\n))
                       (insert ?\ ))
                   (backward-char))
                  (t (backward-char)))))))))

(defun turing (n)
  (interactive "p")
  (cond ((= n 1) (one-transition))
        (t (while (> n 0)
             (one-transition)
             (sit-for 0)
             (setq n (1- n))))))

(global-set-key "\C-t" 'turing)

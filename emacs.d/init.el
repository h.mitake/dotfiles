;; init.el ... loaded first

(show-paren-mode t)
(global-font-lock-mode t)

;; misc settings
(display-time-mode 1)
(line-number-mode 1)
(column-number-mode 1)
;; (display-battery-mode 1)

;; use ibuffer as default
(require 'ibuffer)
(global-set-key "\C-x\C-i" 'ibuffer)
(global-set-key [f2] 'ibuffer)

;; binding dired to F3
(require 'dired)
(global-set-key [f3] 'dired)

(setq transient-mark-mode t)
(setq make-backup-files nil)
(setq auto-save-default nil)

;; hide password
(add-hook 'comint-output-filter-functions
          'comint-watch-for-password-prompt)
(setq no-newline t)

;; vi like cursor moving
(global-set-key "\C-h" 'backward-char)
(global-set-key "\C-j" 'next-line)
(global-set-key "\M-n" 'next-line)
(global-set-key "\C-k" 'previous-line)
(global-set-key "\M-p" 'previous-line)
(global-set-key "\C-l" 'forward-char)

(global-set-key "\C-b" 'help)
;; (global-set-key "\C-n" 'eval-print-last-sexp)
(global-set-key "\C-p" 'kill-line)
(global-set-key "\C-f" 'recenter)

(global-set-key "\M-h" 'delete-backward-char)

;; use system clipboard
(global-set-key "\C-w" 'clipboard-kill-region)
(global-set-key "\M-w" 'clipboard-kill-ring-save)

;; hide tool bar and menu bar and scroll bar
(tool-bar-mode -1)
(menu-bar-mode -1)

(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward-angle-brackets)

(defun my-elisp-dir (dir)
  (concat "~/.emacs.d/" dir))
(defun add-elisp-path (dir)
  (setq load-path (cons (expand-file-name (my-elisp-dir dir)) load-path)))

(add-elisp-path "")

;; auto-save-buffers.el from misc.el
;; based on http://0xcc.net/misc/auto-save/
(require 'auto-save-buffers)
(run-with-idle-timer 0.5 t 'auto-save-buffers) 
(define-key ctl-x-map "as" 'auto-save-buffers-toggle)

;; wordnet.el
(require 'wordnet)
(global-set-key (kbd "M-s") 'wordnet-search)

;; undo-tree
(require 'undo-tree)
(global-undo-tree-mode)

;; underline
(require 'hl-line)
(setq hl-line-face 'underline)
(global-hl-line-mode)

;; linum
(require 'linum)
(global-linum-mode t)

;; yatex
(add-elisp-path "yatex/")
(setq auto-mode-alist (cons (cons "\\.tex$" 'yatex-mode) auto-mode-alist))
(setq auto-mode-alist (cons (cons "\\.sty$" 'yatex-mode) auto-mode-alist))
(setq auto-mode-alist (cons (cons "\\.cls$" 'yatex-mode) auto-mode-alist))
(autoload 'yatex-mode "yatex" "Yet Another Latex mode" t)

;; git
(require 'git)
(require 'git-blame)

(load "proglang.el")

;; wanderlust
(add-elisp-path "apel")
(require 'product)

(add-elisp-path "semi")

(add-elisp-path "flim")
(require 'luna)

(add-elisp-path "wanderlust/elmo")
(add-elisp-path "wanderlust/wl")

(setq ssl-certificate-verification-policy 1) 
(autoload 'wl "wl" "Wanderlust" t)
(autoload 'wl-other-frame "wl" "Wanderlust on new frame." t)
(autoload 'wl-draft "wl-draft" "Write draft with Wanderlust." t)

(setq inhibit-startup-message t)  ; disable startup message

(define-key global-map "\C-o" 'dabbrev-expand)


(setq load-path (cons "/usr/share/emacs/site-lisp/notmuch/" load-path)) ; FIXME...
(autoload 'notmuch "notmuch" "notmuch mail" t)

(defun sign-off ()
  (interactive)
  (insert "Signed-off-by: Hitoshi Mitake <mitake.hitoshi@lab.ntt.co.jp>"))

; You need "xsel" program.
 
(defun x-paste ()
  "insert text on X11's clipboard to current buffer."
  (interactive)
  (insert-string (shell-command-to-string "xsel -b")))
 
(defun x-copy ()
  "copy text on local kill-ring to X11's clipboard."
  (interactive)
  (copy-region-as-kill (point) (mark t))
  (let ((process-connection-type nil))
      (let ((proc (start-process "xsel" "*Messages*" "xsel" "-i" "-b")))
        (process-send-string proc (car kill-ring))
        (process-send-eof proc))))

;; proof general
(setq coq-prog-name "/usr/bin/coqtop")

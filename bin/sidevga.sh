#! /bin/bash

case $1 in
    'on')
	xrandr --output LVDS1 --mode 1024x768
	xrandr --auto --output VGA1 --mode 1024x768
	;;
    'off')
	xrandr --output LVDS1 --mode 1280x800
	;;
    *)
	echo "on/off"
esac